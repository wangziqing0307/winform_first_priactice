﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            var rest = MessageBox.Show("成功", "登入界面", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (rest == DialogResult.Yes)
            {
               
                MessageBox.Show("登录成功");
            }
            else
            {
                MessageBox.Show("登录失败");
                this.Close();
            }
        }

    }
}
